
<?php
$address = isset($_POST['address']) ? $_POST['address']:null;
$image  = isset($_POST['image']) ? $_POST['image']:null;
    $usernameErr = $genderErr = $facultyErr = $birthdayErr = "";
    $username = $gender = $faculty = $birthday = $address = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["username"])) {
        $usernameErr = "Hãy nhập tên.";
        } else {
        $username = test_input($_POST["username"]);
        }
        if (empty($_POST["gender"])) {
            $genderErr = "Hãy chọn giới tính.";
        } else {
            $gender = test_input($_POST["gender"]);
        }
        if (strlen($_POST["faculty"])===0) {
            $facultyErr = "Hãy chọn phân khoa.";
        } else {
            $faculty = test_input($_POST["faculty"]);
        }
        if (empty($_POST["birthday"])) {
            $birthdayErr = "Hãy nhập ngày sinh.";
        }else {
            if(!validateDate($_POST["birthday"])){
            $birthdayErr = "Hãy nhập ngày sinh đúng định dạng.";
            }else {
            $birthday = test_input($_POST["birthday"]);
            }
        }
    }

    function validateDate($birthday, $format = 'dd-mm-yyyy') {
    $d = DateTime::createFromFormat($format, $birthday);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $birthday;
    }

    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);                  
        return $data;
    }
    

echo
    "
		<head>
            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<link rel='stylesheet' type='text/css' href='form_w5.css'>
		</head>
		
		<body>
            <div>
                <form method='post' action='' enctype='multipart/form-data'>
                    <div>
                        <div id='input'>
                            <label id='label'>Họ và tên <span>*<span></label>
                            <b id='post_data'>".$_POST['username']."</b><br/>";
                                if (!empty( $errors['username']['required'])) {
                                    echo '<span id=\'errors\'>'.$errors['username']['required'].'</span>';
                                }
                        echo"    
                        </div>
                        
                        <div id='input'>
                            <label id='label'>Giới tính<span>*<span></label>
                            <b id ='post_data'>". $_POST['gender']."</br>";
                            if (!empty( $errors['gender']['required'])) {
                                echo '<span id=\'errors\'>'.$errors['gender']['required'].'</span>';
                            }
                            
                        echo
                        "</div>
                        <div id='input'>
                            <label id='label'>Phân khoa<span>*<span></label>
                            <b id='post_data'>". $_POST['faculty']."</b></br>";
                            if (!empty( $errors['faculty']['required'])) {
                                echo '<span id=\'errors\'>'.$errors['faculty']['required'].'</span>';
                            }
echo"
                        </div>
                        <div id='input'>
                                <label id='label'> Ngày sinh<span>*<span></label>
                                <b id='post_data'>". $_POST['birthday'] ."</b></br>";
                                if (!empty( $errors['birthday']['required'])) {
                                    echo '<span id=\'errors\'>'.$errors['birthday']['required'].'</span>';
                                }
echo "
                        </div>

                        <div id='input'>
                            <label id='label'> Địa chỉ</label>
                            <b id='post_data'>".$_POST['address']."</b></br>
                        </div>

                        <div id='input'>
                            <label id='label'> Hình ảnh </label>
                            <b>".$_POST['image']."</b>.<br> 
                        </div>
                    </div>

                    <div>
                        <button type='submit'>Xác nhận</button>
                    </div>

                </form>
            </div>
		</body>
	"
?>
